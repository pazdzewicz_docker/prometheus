# Prometheus Docker

Ready-to-run Prometheus Docker Umgebung.

## Setup
### 1. Environment erstellen:

Wir kopieren uns das `.env.template` nach `.env` um dann die Konfigurationsvariablen zu vergeben.

```
cp .env.template .env
cp prometheus.yml.template prometheus.yml

nano .env
nano prometheus.yml
```

Nun die Variablen anhand der Dokumentation und Kommentare anpassen.

### 2. Docker

Jetzt können wir unsere Docker Umgebung starten. Bitte beachte das der Traefik Server gestartet sein muss, sonst erhältst du eine Netzwerk Fehlermeldung von Docker.

```
docker-compose pull
docker-compose up -d
```

### 3. Fertig

Nun können wir unser Grafana so konfigurieren das er die Daten vom Prometheus abruft.